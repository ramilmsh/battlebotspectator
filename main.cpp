/*
 * This code assumes existance of in.json
 * The output goes into out.json
 */

#include <iostream>
#include <stdio.h>
#include <fstream>
/*
 * Using nlohmann/json (https://github.com/nlohmann/json)
 * It is highly advisable to read it up, before reding the code
 */
#include "json.hpp"

using json = nlohmann::json;
using namespace std;

const char IN_PATH[100] = "in.json";
const char OUT_PATH[100] = "out.json";

struct Entry {
    // Team number
    int ind;
    // New positions
    json data;
};

class State {

private:
    json jsonData;
    FILE* file = fopen(OUT_PATH, "w");

public:
    State();
    State(string _data);
    void changeEntries(int n, Entry* entries);
    json data();
    void data(string data);

};

State botState;

int main(int argc, char** argv) {

    ifstream file(IN_PATH);
    string dataString((istreambuf_iterator<char>(file)), (istreambuf_iterator<char>()));

    botState.data(dataString);


    /*
     * changeEntries usage example
     */

    Entry _t;
    _t = {ind: 1, data: {{"x", 1}, {"y", 10}}};

    botState.changeEntries(1, &_t);

    return 0;
}

State::State() {}

State::State(string _data) {
    State::data(_data);
};

/*
 * Accepts an array of Entries to be changed (or a pointer to an entry)
 * n is number of entries (didn't find any function to get it)
 */
void State::changeEntries(int n, Entry* entries) {

    for (int i = 0; i < n; ++i) {

        Entry entry = entries[i];
        jsonData[entry.ind] = {{"x", entry.data["x"]}, {"y", entry.data["y"]}};

    }

    fputs(jsonData.dump().c_str(), file);

    return;
}

void State::data(string data) {
    jsonData = json::parse(data);
}

json State::data() {
    return jsonData;
}