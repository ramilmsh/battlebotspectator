var http = require('http'),
    fs = require('fs'),
    data = fs.readFileSync('./out.json', 'utf8').split('\n')[1],
    indexFile = fs.readFileSync('./static/index.html'),
    mainJS = fs.readFileSync('./static/main.js');

function updateData() {
    fs.readFile('./out.json', 'utf8', function (err, file) {
        data = file;
    });
}

var int = setInterval(updateData, 1000);

var server = http.createServer(handler);

function handler(req, res) {
    switch (req.url) {
        case '/':
            res.end(indexFile, {status: 200});
            break;

        case '/main.js':
            res.end(mainJS, {status: 200});
            break;

        case '/get':
            res.end(data, {status: 200});
            break;

        default:
            res.end('404');
    }
}

server.listen(3000, function (err) {
    if (err) throw err;

    console.log('Server running');
});
