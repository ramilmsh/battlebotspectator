// Reader Code

const BOT_WIDTH = 15;
const BOT_LENGTH = 20;
const BOARD_WIDTH = 700;
const BOARD_HEGHT = 700;
const COLORS = ["#FF0000", "#00FF00", "#0000FF", "#FFFF00", "#FF00FF", "#00FFFF"];
var xhttp = new XMLHttpRequest(), int, data = {},
    canvas = document.getElementById("myCanvas"),
    ctx = canvas.getContext("2d");
ctx.fillStyle = 'fff';
canvas.width = BOARD_WIDTH;
canvas.height = BOARD_HEGHT;

function initScreen() {
    canvas.style.marginLeft = (window.innerWidth > BOARD_WIDTH ? (window.innerWidth - BOARD_WIDTH) / 2 : 0) + 'px';
    canvas.style.marginTop = (window.innerHeight > BOARD_HEGHT ? (window.innerHeight - BOARD_HEGHT) / 2 : 0) + 'px';
}

initScreen();

function read(callback) {
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200)
            callback(this.responseText);
    }
    xhttp.open("GET", "/get", true);
    xhttp.send();
}

window.onload = function () {
    int = setInterval(draw, 1000);
}

window.onresize = initScreen;

function draw() {
    read(function (data) {
        Object.freeze(COLORS);
        // var testData = '{  "bots": [    {      "x": 23,      "y": 71,      "angle": 45,      "id": 0    },    {      "x": 69,      "y": 11,      "angle": 95,      "id": 1    },    {      "x": 123,      "y": 21,      "angle": 145,      "id": 3    },    {      "x": 223,      "y": 41,      "angle": 255,      "id": 4    }  ]}';
        var response = JSON.parse(data);


        ctx.clearRect(0, 0, canvas.width, canvas.height);
        function drawBot(bot, id) {
            var centerX = bot.x + (BOT_WIDTH / 2);
            var centerY = bot.y + (BOT_LENGTH / 2);

            ctx.save();
            ctx.translate(centerX, centerY);
            ctx.rotate((90 + bot.angle)*Math.PI/180);

            ctx.fillStyle = COLORS[id];
            ctx.fillRect(-(BOT_WIDTH / 2), -(BOT_LENGTH / 2), BOT_WIDTH, BOT_LENGTH);

            ctx.restore();

            // draws a dot in the "actual" location of the bot due to slight error in canvas rotation
            ctx.fillStyle = "#000";
            ctx.fillRect(bot.x - 1, bot.y - 1, 3, 3);
        }

        for (var i = 0; i < response.length; i++) {
            var specs = response[i];
            drawBot(specs, i);
        }
    });
}


